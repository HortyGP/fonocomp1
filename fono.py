from csv import reader            #aqui está importando da biblioteca cvs o modulo reader
import matplotlib.pyplot as plt   #aqui está importando da biblioteca matplotlib.pyplot com o modulo plt

class Iris:                       #classe chamada Iris
    def __init__(self):           #função init
        self.data_file = reader(open('fonocomp'), delimiter="\t")  #aqui está abrindo o arquivo fonocomp
        self.data = [linha for linha in self.data_file]
        self.setosa = [linha for linha in self.data if 'setosa' in linha[-1]]
        self.versicolor = [linha for linha in self.data if 'versicolor' in linha[-1]]
        self.virginica = [linha for linha in self.data if 'virginica' in linha[-1]]
        self.zdata = list(zip(*self.data))
        self.zset = list(zip(*self.setosa))
        self.zver = list(zip(*self.versicolor))
        self.zvir = list(zip(*self.virginica))
        print(self.data)

    def plot(self, x=0, y=1):
        plt.scatter(self.zset[x], self.zset[y], color='red')
        plt.scatter(self.zver[x], self.zver[y], color='blue')
        plt.scatter(self.zvir[x], self.zvir[y], color='green')
        plt.show()


if __name__ == '__main__':
    Iris().plot(1, 3)   #aqui está chamando a classe Iris colocando no plot os valores x=1 e y=3
