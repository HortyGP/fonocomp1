import matplotlib.pyplot as plt
from matplotlib.pyplot import bar, show
from json import loads  # utilizou o loads para pegar todas as coisas do jason que estão vindo em string
from urllib.request import urlopen  # bibliioteca para licar com localizadores universais
                                    # Erequest é um pedido para esse recurso  #urlopen: vai abrir o recurso
from pickle import dump, load
from numpy import average
from datetime import datetime

M_S__F = '%Y-%m-%d %H:%M:%S.%f'

URL = "http://activufrj.nce.ufrj.br/api/getlist"
URLREG = "http://activufrj.nce.ufrj.br/api/getsession?id="


class Activ:
    def __init__(self):  # underline em python quer dizer que é um nome reservado, um nome com significado predefinido
        self.jogadores = None

    def one_player(self, play_url='90235c60e284c8ec5b8fc4107300f398'):
        dataset = urlopen(URL)  # por favor abra o pacote
        pyset = loads(dataset.read())  # vai transformar os strings do json
        registros = pyset['applist']  # registro principal que esta com o Mauricio
        registros2018 = [numreg for data, numreg in registros if data and "2018" in data]
        print(len(registros2018), registros2018)
        urlreg1 = URLREG + play_url # vai somar colocando no link
        pass
        aluno1 = urlopen(urlreg1)
        pyset = loads(aluno1.read())
        print(len(pyset), pyset["session"])
        jogador = Jogador(games=pyset["games"], **pyset["session"])
        dump(jogador, pic)
        pic = open("jogador.plk", "rb")
        jogador = load(pic)
        self.jogadores = [jogador]
        #print(list(pyset["games"][0]["goal"][0]["trial"][0][0].keys()))
        print(jogador.games[0].goal[0].trial[0].house)

    def get_jogadores(self):
        def load_registros():
            dataset = urlopen(URL)  # por favor abra o pacote
            pyset = loads(dataset.read())  # vai transformar os strings do json
            registros = pyset['applist']  # registro principal que esta com o Mauricio
            registros2018 = [numreg for data, numreg in registros if data and "2018" in data]
            print(len(registros2018), registros2018)
            return registros2018

        def get_jogador(urlreg):
            urlreg1 = URLREG + urlreg
            aluno1 = urlopen(urlreg1)
            pyset = loads(aluno1.read())
            print(len(pyset), pyset["session"])
            jogador = Jogador(games=pyset["games"], **pyset["session"])
            return jogador
        pic = open("jogador.plk", "wb")
        self.jogadores = [get_jogador(jog_url) for jog_url in load_registros()]
        dump(self, pic)

    @staticmethod
    def load_jogadores():
        pic = open("jogador.plk", "rb")
        return load(pic)

    def get_gamers(self, name="tol"):
        return {ind: [game for game in player.games if game.has_game(name)]for ind, player in enumerate(self.jogadores)}


class Trial:
    def __init__(self, xpos, house, ypos, player, state, score, result, time,
                 marker, categoria=0, cor=0, acertos=0, outros=0, forma=0, numero=0, carta_resposta=0):
        self.xpos, self.house, self.ypos, self.player, self.state, self.score, self.result, self.time, self.marker = \
            xpos, house, ypos, player, state, score, result, time, marker
        self.categoria = categoria
        self.cor, self.acertos, self.outros, self.forma, self.numero, self.carta_resposta =\
            cor, acertos, outros, forma, numero, carta_resposta
        print("      Trial", xpos, house, ypos, player, state, score, result, time)


class Gol:
    def __init__(self, houses, criteria, markers, trial, headings, time, level):
        self.houses, self.criteria, self.markers, self.trial, self.headings, self.time, self.level =\
            houses, criteria, markers, trial, headings, time, level
        print("   Gol", houses, criteria, markers, headings, time, level)
        if trial:
            self.trial = [[Trial(**params) for params in a_trial] for a_trial in trial]


class Games:
    def __init__(self, maxlevel, goal, name, time):
        self.maxlevel, self.goal, self.name, self.time = maxlevel, goal, name, time
        print("  Games", maxlevel, name, time)
        self.goal = [Gol(**params) for params in goal]

    def has_game(self, name):
        return name == self.name

    def show_game(self):
        return len(self.goal), [[len(trial) for trial in goal.trial] for goal in self.goal if goal.trial],\
               [[(trial[0].time, trial[-1].time) for trial in goal.trial if trial] for goal in self.goal if goal.trial]


class Jogador:
    def __init__(self, starttime, idade1, idade2, ano1, ano2, escola, sexo1, sexo2,
                 tipoescola, endtime, games):
        self.starttime = starttime
        self.idade1 = idade1
        self.idade2 = idade2
        self.ano1 = ano1
        self.ano2 = ano2
        self.escola = escola
        self.sexo1 = sexo1
        self.sexo2 = sexo2
        self.tipoescola = tipoescola
        self.endtime = endtime
        print("Jogador", starttime, idade1, idade2, ano1, ano2, escola, sexo1, sexo2, tipoescola, endtime)
        self.games = [Games(**params) for params in games]


def main():
    activ = Activ.load_jogadores()
    print(activ.jogadores[0].games[0].goal[0].trial[0][0].house)
    x_y_axis = []
    x_y_trials = []
    x_y_time = []
    x_y_sexo1 = []
    for k, i in activ.get_gamers().items():
        if i and i[0].goal and i[0].goal[0].trial:
            x_y_axis.append((k,i[0].show_game()[0]))
            x_y_sexo1.append((k+0.4,i[0].show_game()[0]))
            trials = [i[0] for i in i[0].show_game()[1]]
            x_y_trials.append((k+0.3, average(trials)))
            x_y_time0, x_y_time1 = i[0].show_game()[-1][0][0]
            interval = datetime.strptime(x_y_time1, M_S__F) - datetime.strptime(x_y_time0, M_S__F)
            x_y_time.append((k+0.6, interval.seconds/10))
            interval.seconds

            print("jogador", k, i[0].show_game(), interval.seconds)

    x_axis, y_axis, = zip(*x_y_axis)
    x_axis_t, y_trials, = zip(*x_y_trials)
    x_axis_i, y_time, = zip(*x_y_time)
    x_axis_i, y_sexo1, = zip(*x_y_sexo1)
    bar_color = 'blue'

    plt.bar(x_axis, y_axis, width=0.25, color=bar_color)
    plt.bar(x_axis_t, y_trials, width=0.25, color='red')
    plt.bar(x_axis_i, y_time, width=0.25, color='green')
    plt.bar(x_axis_i, y_sexo1, width=0.25, color='black')
    plt.title('TORRE DE LONDRES')
    plt.xlabel('JOGADORES')
    plt.ylabel('JOGADAS, MEDIA DE TENTATIVAS, TEMPO')
    plt.show()
    return
"""
    for k, i in activ.get_gamers().items():
        if i and i[0].goal and i[0].goal[0].trial:
            a_goal = i[0].goal
            for each in a_goal:
                print("  goal", each.level)
                if not each.trial:
                    continue
                for turn in each.trial[0]:
                    print("      jogada", turn.house, turn.marker, turn.time)
                # print(k, i[0].goal[0].trial[0][0].house if i[0].goal[0].trial else [])
"""
if __name__ == '__main__':
    #Activ().get_jogadores() #só descomentar quando for recarregar o banco de dados do Activ
    main()
