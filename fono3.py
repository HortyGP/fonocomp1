import matplotlib.pyplot as plt
y_axis = [20,50,30]
x_axis = range(len(y_axis))
width_n = 0.4
bar_color = 'yellow'
plt.title("Grafico de Barras")
plt.bar(x_axis, y_axis, width=width_n, color=bar_color, align='center')
plt.show()