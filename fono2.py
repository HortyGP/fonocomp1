import matplotlib.pyplot as plt
plt.plot([10,20,30,40], [15, 40, 75, 90], linestyle='--', color='r', marker='s',
         linewidth=3.0)
plt.axis([0,50,0,100])
plt.show()